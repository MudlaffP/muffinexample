import os

from peewee import PostgresqlDatabase

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SOCKETS = []

NAME = "Muffin Example (ME)"
# Muffin
PLUGINS = (
    'muffin_jade',
    'muffin_jinja2',
    'muffin_session',
    'muffin_admin',
)

STATIC_FOLDERS = os.path.join(BASE_DIR, 'static')

JADE_CACHE_SIZE = 100
JADE_ENCODING = 'UTF-8'
JADE_PRETTY = True
JADE_TEMPLATE_FOLDERS = [os.path.join(BASE_DIR, 'jade_templates')]

# Plugin options
SESSION_SECRET = 'SuperSecret!'

DATABASE_NAME = 'database_name'
DATABASE_USER = 'database_user'
DATABASE_PASSWORD = 'super_secret_password'
DATABASE_HOST = 'host'
DATABASE_PORT = '5432'

PEEWEE_MIGRATIONS_PATH = os.path.join(BASE_DIR, 'migrations')
PEEWEE_MIGRATIONS_ENABLED = True

DB_CONN = PostgresqlDatabase(
    database=DATABASE_NAME,
    user=DATABASE_USER,
    password=DATABASE_PASSWORD,
    host=DATABASE_HOST,
    port=DATABASE_PORT
)
