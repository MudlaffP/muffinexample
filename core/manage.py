"""
    Applications commands
"""
import datetime as dt
from core import app


def _create_user(username=None, email=None, password=None, is_super=False):
    from mixer.backend.peewee import Mixer
    from core.models import User
    from muffin.utils import generate_password_hash

    if username and email and password:
        mixer = Mixer(commit=True)
        mixer.guard(User.email == email).blend(
            User,
            username=username,
            email=email,
            password=generate_password_hash(password),
            is_super=is_super
        )


@app.manage.command
def create_user(username: str, email: str, password: str):
    _create_user(username, email, password)


@app.manage.command
def create_super_user(username: str, email: str, password: str):
    """
    Create application super user

    All arguments are required so we pass them without flags f.e.:

    create_super_user SomeUserName Some@ema.il some_password

    :param username: Username for user
    :param email: email for user
    :param password: user password
    """
    _create_user(username, email, password, True)


@app.manage.command
def make_migration(name: str=None, fake: bool=False):
    """
    Run application's migrations.
    :param name: Choose a migration' name
    :param fake: Run as fake. Update migration history and don't touch the database
    """
    from peewee_migrate import Router
    from project_settings.settings import DB_CONN, PEEWEE_MIGRATIONS_PATH

    router = Router(DB_CONN, migrate_dir=PEEWEE_MIGRATIONS_PATH)

    router.run(name, fake)


@app.manage.command
def create_migration(name: str=None, auto: bool=False):
    """
    Create a migration.
    :param name: Set name of migration [auto]
    :param auto: Track changes and setup migrations automatically
    """
    from peewee_migrate import Router
    from muffin.utils import Struct

    from project_settings.settings import DB_CONN, PEEWEE_MIGRATIONS_PATH

    router = Router(DB_CONN, migrate_dir=PEEWEE_MIGRATIONS_PATH)
    models = Struct()

    if not name:
        name = dt.datetime.now().strftime("%Y%m%d%H%M%S")

    if auto:
        auto = list(models.values())
    router.create(name, auto)


@app.manage.command
def rollback_migration(name: str):
    """
    Rollback a migration.
    :param name: Migration name (actually it always should be a last one)
    """
    from peewee_migrate import Router

    from project_settings.settings import DB_CONN, PEEWEE_MIGRATIONS_PATH

    router = Router(DB_CONN, migrate_dir=PEEWEE_MIGRATIONS_PATH)
    router.rollback(name)
