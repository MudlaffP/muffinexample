import muffin

from core import app
from core.models import User


# Register custom context provider
# could be a function/coroutine
@app.ps.jade.ctx_provider
def custom_context():
    return {'VAR': 'VALUE'}


# Setup an user loader
@app.ps.session.user_loader
def get_user(user_id):
    """ This provides a user loading procedure to the application. """
    return User.get(User.id == user_id)


# Setup endpoints
@app.register('/')
def index(request):
    """ Get a current logged user and render a template to HTML. """
    user = yield from app.ps.session.load_user(request)
    return app.ps.jade.render('Index.jade', user=user, view='index')


# app.register supports any regexp in paths
@app.register('/login/?', methods='POST')
def login(request):
    """ Implement user's login. """
    data = yield from request.post()
    user = User.select().where(User.email == data.get('email')).get()
    if user.check_password(data.get('password')):
        yield from app.ps.session.login(request, user.pk)

    return muffin.HTTPFound('/')


@app.register('/logout')
def logout(request):
    """ Implement user's logout. """
    yield from app.ps.session.logout(request)
    return muffin.HTTPFound('/')


# app.register supports multi paths
@app.register('/profile', '/auth')
# ensure that request user is logged to the application
@app.ps.session.user_pass(lambda u: u, '/')
def profile(request):
    return app.ps.jade.render('Profile.jade', user=request.user, view='profile')


# Sockets
@app.register('/ws')
def websocket(request):
    from project_settings.settings import SOCKETS

    user = yield from app.ps.session.load_user(request)
    user = user and user.username or 'anonimous'
    ws = muffin.WebSocketResponse()
    ws.start(request)
    for ws_ in SOCKETS:
        ws_.send_str('%s joined' % user)

    SOCKETS.append(ws)

    while True:
        msg = yield from ws.receive()
        if msg.tp == muffin.MsgType.text:
            for ws_ in SOCKETS:
                if ws_ is not ws:
                    ws_.send_str("%s: %s" % (user, msg.data))
            continue
        break

    SOCKETS.remove(ws)
    for ws_ in SOCKETS:
        ws_.send_str('%s disconnected' % user)

    return ws
