import datetime as dt
import peewee as pw
from muffin.utils import generate_password_hash, check_password_hash

from project_settings.settings import DB_CONN


class BaseModel(pw.Model):
    """A base model that will use our Postgresql database"""
    class Meta:
        database = DB_CONN


class User(BaseModel):
    """
    Application user model definition
    """
    created = pw.DateTimeField(default=dt.datetime.now)
    username = pw.CharField()
    email = pw.CharField(unique=True)
    password = pw.CharField()
    is_super = pw.BooleanField(default=False)

    def __unicode__(self):
        return self.email

    @property
    def pk(self):
        return self.id

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(password, self.password)
