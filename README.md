# Description

I wanted to check [Muffin](https://github.com/klen/muffin). I tested it with Postgres DB and [muffin-jade](https://github.com/klen/muffin-jade).

To run this project please install requirements and update ```project_settings.settings``` database fields to correspond your Postgres DB.

If you wish to work on SQLite you could use [muffin-peewee](https://github.com/klen/muffin-peewee)

Templates are translated from example Muffin project to jade.

# Command to create user

    $ muffin core:app create_user
    
    $ muffin core:app create_super_user

# Command to run

    $ muffin core run --bind=0.0.0.0:8000 --config=project_settings.settings

# Commands for migrations

    $ muffin core:app create_migration
    
    $ muffin core:app make_migration
    
    $ muffin core:app rollback_migration
    